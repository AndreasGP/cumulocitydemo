package madp.ut.cumulocitysensordemo;

import android.os.Build;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import madp.ut.cumulocitysensordemo.rest.RestService;

public class RegistrationActivity extends AppCompatActivity {

    private static final String TAG = "REGISTRATION_ACTIVITY";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
    }

    @Override
    public void onResume() {
        super.onResume();

        registrationCycle();
    }

    protected void registrationCycle(){
        boolean isRegistered = isDeviceRegistered();

        if(isRegistered){
            //TODO
        }else{
            createDevice();
        }
    }


    // STEP 1: CHECK IF THE DEVICE IS ALREADY REGISTERED
    private boolean isDeviceRegistered(){
        // Following two lines are needed for rest test
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        String c8y_hardware_serial = Build.SERIAL;
        int response = RestService.performGETRequestForResponseCode("/identity/externalIds/c8y_Serial/" + c8y_hardware_serial);

        switch(response){
            case 200:
                //TODO https://code.tutsplus.com/tutorials/android-from-scratch-using-rest-apis--cms-27117
                Log.i(TAG, "registered");
                return true;
            case 401:
                //TODO handle
                Log.e(TAG, "wrong credentials");
                break;
            case 404:
                Log.e(TAG, "wrong URL");
                break;
            case 808:
                Log.i(TAG, "device not registered");
                return false;
            default:
                Log.e(TAG, "unexpected result in registration check");
        }
        return false;
    }

    // STEP 2: CREATE THE DEVICE IN THE INVENTORY
    private void createDevice(){
        String c8y_hardware_name = Build.MODEL;
        String c8y_hardware_serial = Build.SERIAL;
        String c8y_hardware_revision = Build.VERSION.RELEASE;

        //TODO find sensors and stuff

        int deviceId = RestService.performPOSTDeviceCreation(c8y_hardware_name, c8y_hardware_serial, c8y_hardware_revision);

        if(deviceId != 1){
            // STEP 3: REGISTER THE DEVICE
            Log.i(TAG, "Created device with id:" + deviceId);
            String externalId = RestService.performPOSTDeviceRegistration(c8y_hardware_serial, String.valueOf(deviceId));
        }else{
            //TODO bad reg
        }

    }
}
