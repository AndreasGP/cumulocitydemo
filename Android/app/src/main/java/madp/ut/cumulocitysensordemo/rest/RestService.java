package madp.ut.cumulocitysensordemo.rest;

import android.content.Context;
import android.util.JsonReader;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

import javax.net.ssl.HttpsURLConnection;

import madp.ut.cumulocitysensordemo.R;

/**
 * Created by Madis-Karli Koppel on 24.09.2017.
 */

public class RestService {

    private static final String SITE_URL = "https://testing.iot.cs.ut.ee";
    private static final String TAG = "REST_SERVICE";

    public static int performPOSTDeviceCreation(String c8y_hardware_name, String c8y_hardware_serial, String c8y_hardware_revision){
        Log.i(TAG, "Begin POST creation request");

        try {
            // Create the device

            JSONObject body = new JSONObject();
            body.put("name", c8y_hardware_name + " " + c8y_hardware_serial);
            body.put("type", "Android Phone"); // was c8y_Linux
            body.put("c8y_IsDevice", "{}");


            JSONObject hardware = new JSONObject();
            hardware.put("model", c8y_hardware_name);
            hardware.put("serialNumber", c8y_hardware_serial);
            hardware.put("revision", c8y_hardware_revision);

            body.put("c8y_Hardware", hardware);


            body.put("c8y_SupportedMeasurements", "[c8y_TemperatureMeasurement]");
            // TODO add more stuff

            // Send device creation request
            URL url = new URL(String.format(SITE_URL + "/inventory/managedObjects"));

            HttpsURLConnection connection =
                    (HttpsURLConnection) url.openConnection();
            connection.setRequestProperty("Authorization", "Basic bWFkcGRlbW86bWFkcDIwMTc=");

            connection.setRequestMethod("POST");
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type","application/json");
            connection.setRequestProperty("Accept","application/json");

            OutputStream outputStream = connection.getOutputStream();
            outputStream.write(body.toString().getBytes());
            outputStream.flush();
            outputStream.close();

            // Get the response
            Log.i(TAG, "sent "+ String.valueOf(body.toString()));
            Log.i(TAG, "response "+ String.valueOf(connection.getResponseCode()));


            InputStream errorStream = connection.getInputStream();
            InputStreamReader responseBodyReader =
                    new InputStreamReader(errorStream, "UTF-8");

            JsonReader jsonReader = new JsonReader(responseBodyReader);

            jsonReader.beginObject();
            while (jsonReader.hasNext()) {
                String key = jsonReader.nextName();
                if (key.equals("id")) {
                    String value = jsonReader.nextString();
                    return Integer.valueOf(value);
                } else {
                    jsonReader.skipValue();
                }
            }
            jsonReader.close();

            return -1;

        }catch(IOException e){
            //TODO handle
            Log.e(TAG, Log.getStackTraceString(e));
            return -1;
        }
        catch(JSONException e){
            //TODO handle
            Log.e(TAG, Log.getStackTraceString(e));
            return -1;
        }
    }

    public static String performPOSTDeviceRegistration(String c8y_hardware_serial, String id){
        Log.i(TAG, "Begin POST creation request");

        try {
            // Create the device

            JSONObject body = new JSONObject();
            body.put("externalId", c8y_hardware_serial);
            body.put("type", "c8y_Serial"); // was c8y_Linux


            // Send device creation request
            URL url = new URL(String.format(SITE_URL + "/identity/globalIds/" + id + "/externalIds"));

            HttpsURLConnection connection =
                    (HttpsURLConnection) url.openConnection();
            connection.setRequestProperty("Authorization", "Basic bWFkcGRlbW86bWFkcDIwMTc=");

            connection.setRequestMethod("POST");
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type","application/json");
            connection.setRequestProperty("Accept","application/json");

            OutputStream outputStream = connection.getOutputStream();
            outputStream.write(body.toString().getBytes());
            outputStream.flush();
            outputStream.close();

            // Get the response
            Log.i(TAG, "sent "+ String.valueOf(body.toString()));
            Log.i(TAG, "response "+ String.valueOf(connection.getResponseCode()));


            InputStream errorStream = connection.getInputStream();
            InputStreamReader responseBodyReader =
                    new InputStreamReader(errorStream, "UTF-8");

            JsonReader jsonReader = new JsonReader(responseBodyReader);

            jsonReader.beginObject();
            while (jsonReader.hasNext()) {
                String key = jsonReader.nextName();
                if (key.equals("externalId")) {
                    String value = jsonReader.nextString();
                    Log.i(TAG, value);
                } else {
                    jsonReader.skipValue();
                }
            }
            jsonReader.close();

            return "-1";


        }catch(IOException e){
            //TODO handle
            Log.e(TAG, Log.getStackTraceString(e));
            return "-1";
        }
        catch(JSONException e){
            //TODO handle
            Log.e(TAG, Log.getStackTraceString(e));
            return "-1";
        }
    }

    public static int performGETRequestForResponseCode(String resourceURL) {
        Log.i(TAG, "Begin GET isRegistered request");

        try {
            URL url = new URL(String.format(SITE_URL + resourceURL));

            HttpsURLConnection connection =
                    (HttpsURLConnection) url.openConnection();
            connection.setRequestProperty("Authorization", "Basic bWFkcGRlbW86bWFkcDIwMTc=");

            Log.i(TAG, "response "+String.valueOf(connection.getResponseCode()));

            if (connection.getResponseCode() == 200) {
                return connection.getResponseCode();
            } else {
                //This means that device is not registered or there was an issue

                InputStream errorStream = connection.getErrorStream();
                InputStreamReader responseBodyReader =
                        new InputStreamReader(errorStream, "UTF-8");

                JsonReader jsonReader = new JsonReader(responseBodyReader);

                jsonReader.beginObject();
                while (jsonReader.hasNext()) {
                    String key = jsonReader.nextName();
                    if (key.equals("error")) {

                        String value = jsonReader.nextString();
                        if(value.equals("identity/Not Found")){
                            return 808;
                        }
                        return 808;
                    } else {
                        jsonReader.skipValue();
                    }
                }
                jsonReader.close();

                return connection.getResponseCode(); //SOME ISSUE WITH PAGE
            }
        } catch (IOException e) {
            //TODO handle
            Log.e(TAG, Log.getStackTraceString(e));
            return -1;
        }
    }

    public static JSONObject getJSON() {

        try {
            URL url = new URL(String.format(SITE_URL));
            HttpURLConnection connection =
                    (HttpURLConnection) url.openConnection();

            connection.addRequestProperty("auth", "Basic bWFkcGRlbW86bWFkcDIwMTc=");

            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(connection.getInputStream()));

            StringBuffer json = new StringBuffer(1024);
            String tmp = "";
            while ((tmp = reader.readLine()) != null)
                Log.i(TAG, tmp);
            json.append(tmp).append("\n");
            reader.close();

            JSONObject data = new JSONObject(json.toString());

            // This value will be 404 if the request was not
            // successful
            if (data.getInt("cod") != 200) {
                return null;
            }

            return data;
        } catch (Exception e) {
            Log.e(TAG, Log.getStackTraceString(e));
            return null;
        }
    }

}
